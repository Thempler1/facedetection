import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
@Component({
  selector: 'app-faces',
  templateUrl: './faces.component.html',
  styleUrls: ['./faces.component.scss']
})
export class FacesComponent implements OnInit {

  // video = document.getElementById('video');
  public video: MediaStream;
  // public stream: MediaStream;

  constructor() { }

  public ngOnInit() {
    // navigator.mediaDevices.getUserMedia({ video: true })
    //  .then(stream => {
    //    console.log('> stream', stream);
    //    this.stream = stream;
    //  });
    this.startVideo();
  }

  public startVideo() {
    navigator.mediaDevices.getUserMedia({ video: {} })
      .then(stream => {
        console.log('> stream', stream);
        this.video = stream;
      });
  }
}
