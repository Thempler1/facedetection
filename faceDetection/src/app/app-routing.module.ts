import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { FacesComponent } from './features/faces/faces.component';

const routes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'faces', component: FacesComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
